#Gamesys Ludum Dare 

from 4th November 7:00pm to 6th November 5:30pm
(see test_report.txt for the diary notes)

[Play the game right away !](http://www.lecrabe.net/test/gamesys/)

Intentions
-----

As the game was supposed to involve some gambling, i thought it would be a nice idea to create a game where the user feels that everything is random, but in fact the game should have the ability to decide the player wins or looses.

Most of gambling games calls a server to get in fo before the game begins.

So after forgetting using a physics engine (too risky in 48h), i had the idea of having characters walking in a maze, finding an exit.

With a path finder, it would have been easy to know which character would win.

But due to the lack of time, i only implemented a random walking behavior to the characters, but i am happy i could manage to create this predictive mechanism.

Description
-----

Treasure hunter allows the player to bet 1 coin on a hunter who should be the first one to find the treasure.

If the chosen hunter finds the treasure, the player wins 5 coins.

Tools used
-----

For this project the followinf tools have been used:

* Github/SourceTree for versioning
* Webstorm 11 for development
* Google to search for graphics assets
* Audacity to convert mp3 to ogg
* Texture packer to create the spritesheet

Engine
----
Most of the time has been spent on building an game engine with many features:

### configuration via config.json (dev/bin/assets)
* user credits
* predictive parameters (predict if player can win or not)
* some translations 
* characters behavior params like speed

### graphics

* assets loader
* sprite sheets
* no filters used to be fully canvas compatible

###sounds

* howler sound engine
* loops support


Predictive mode
------------
The character class implements a Step method which is supposed to be called every frame.

When the character has to take a decision (changing direction for now), it calls its behavior to know what to do.

By starting a first simulation mode, the behavior chooses random values as decisions.

When the game starts the behavior just reads those values (replaying the simulation)

By doing this we can predict which character will win before the player chooses a hunter.

Depending on if the player should win, we assign the right hunter skin to characters.

tada !


Installation
------------

* open a terminal and install grunt-cli:

    ```npm install -g grunt-cli```

* import needed libraries to the project:

    ```npm install```

* compile the project

    ```grunt compile```

You can browse 'index.html' for production version (no debug), or 'index_dev.html' for development version (no debug)
You also need a local server to run the index.html page.

TODO
------------

Thanks to this predictive mode, the following features could be implemented:

* create a maze with walls
* finding a exit instead of searching for a treasure
* i wanted character to have other animations that could impact their walking time: like salute each others when they face 
* add more particles effect
* and so on

----
## changelog
* 06-Nov-2015 Arthy
