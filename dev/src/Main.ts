///<reference path="./util/Log.ts" />
///<reference path="./util/Clock.ts" />
///<reference path="./util/signal/SignalManager.ts" />
///<reference path="./game/GameStage.ts" />
///<reference path="./game/screen/MainScreen.ts" />
///<reference path="./game/screen/LoadingScreen.ts" />
///<reference path="./game/screen/InGameScreen.ts" />
///<reference path="./game/screen/EndGameScreen.ts" />
///<reference path="./game/model/enums/ScreenEnum.ts" />
///<reference path="./game/model/Config.ts" />

import GameStage = game.GameStage;
import ScreenEnum = game.model.enums.ScreenEnum;
import Config = game.model.Config;

function init(userObj:any, debug:boolean) {

    var isMobile = false;
    // android smartphones + iphone + ipad
    if (userObj.device.type == "Mobile" || userObj.device.type == "Tablet") isMobile = true;
    else if (userObj.device.type == "Desktop" && userObj.os.family == "Android") {
        // android tablet
        isMobile = true;
    }

    log.Logger.init(debug);
    log.Logger.log("INIT ");

    util.signal.SignalManager.init();
    util.Clock.init();

    GameStage.create(0, true, isMobile);
    GameStage.goToScreen(ScreenEnum.LOADING);
}
