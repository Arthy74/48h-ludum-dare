///<reference path="../signal/SignalManager.ts" />
///<reference path="../signal/CoreSignals.ts" />
///<reference path="./ASoundEngine.ts" />
///<reference path="./howler/HowlerEngine.ts" />
///<reference path="./signal/SoundSignals.ts" />

module util.audio {

    import SignalManager = util.signal.SignalManager;
    import SoundSignals = util.audio.signal.SoundSignals;
    import HowlerEngine = util.audio.howler.HowlerEngine;

    export class SoundManager {

        private static _ready: boolean = false;
        private static _playingBG:any;
        private static _playingBGID:string;
        private static _engine:ASoundEngine;

        public static init(audioJson:Object, isMobile:boolean)
        {
            this._ready = false;

            this._engine = new HowlerEngine();
            this._engine.init(audioJson, isMobile);

            SignalManager.addCallbackSignal(SoundSignals.SOUND_MUTED, this.onMuteSound);
        }

        public static onEngineReady()
        {
            this._ready = true;
            SignalManager.invokeSignal(SoundSignals.SOUNDS_READY);
        }

        public static onMuteSound=(b:boolean)=>
        {
            SoundManager._engine.muteSounds(b);
        }

        public static setMobile()
        {
            this._ready = true;
        }

        public static playGameBG(id:string):void
        {
            if (id == this._playingBGID)
                return;

            if (this._playingBG)
                this._playingBG.stop();

            this._playingBG = this.playSound(id);
            this._playingBGID = id;
        }

        public static playSound(id : string):any
        {
            if (!SoundManager._ready) return;
            return this._engine.playSound(id);
        }
    }
}