///<reference path="../SoundManager.ts" />
///<reference path="../ASoundEngine.ts" />
///<reference path="./HowlerSound.ts" />
///<reference path="./HowlerSoundVO.ts" />
///<reference path="../../Log.ts" />

module util.audio.howler {

    import Logger = log.Logger;

    export class HowlerEngine extends ASoundEngine {

        private _loadingCount: number;
        private _HowlerSounds:Array<HowlerSound>;
        private _formats:Array<string>;

        public init(audioJson:any, isMobile:boolean) {

            Logger.log("INIT Howler JS");

            if (isMobile)
            {
                this._formats = [ 'ogg', 'mp3' ];
            }
            else
            {
                this._formats = [ 'mp3', 'ogg'];
            }

            // Create a single item to load.
            this._HowlerSounds = new Array<HowlerSound>();
            this._loadingCount = 0;

            for (var i = 0, len = audioJson.length; i < len; i++) {
                this._HowlerSounds[ audioJson[i].id ] = new HowlerSound(this, new HowlerSoundVO(audioJson[i], this._formats));
                if (audioJson[i].preload) this._loadingCount++;

                Logger.log("added ", this._HowlerSounds[ audioJson[i].id ], this._loadingCount);
            }

            if (this._loadingCount==0)
            {
                SoundManager.onEngineReady();
            }
        }

        public muteSounds(b:boolean)
        {
            Logger.log("muteSounds ", b);
            for (var z in this._HowlerSounds)
            {
                this._HowlerSounds[z].mute(b);
            }
        }

        public onSoundPreloaded() {
            Logger.log("onSoundPreloaded ", this._loadingCount);
            this._loadingCount--;
            if (this._loadingCount == 0) {
                SoundManager.onEngineReady();
            }
        }

        public playSound( id : string ):HowlerSound
        {
            Logger.log("Howler playing ", id);
            this._HowlerSounds[ id ].play();

            return this._HowlerSounds[ id ];
        }
    }
}