///<reference path="../../lib/PIXI.d.ts" />

module util {

    export class DisposeUtil {

        public static dispose(container:PIXI.DisplayObjectContainer)
        {
            for (var i = 0, len = container.children.length; i < len; i++) {
                if (container.children[i] instanceof PIXI.DisplayObjectContainer)
                    DisposeUtil.dispose(<PIXI.DisplayObjectContainer>container.children[i]);

                if (container.children[i] instanceof PIXI.Text)
                    (<PIXI.Text>container.children[i]).destroy(true);
            }
        }

        public static destroyTexture(txt:PIXI.Texture)
        {
            //PIXI.Texture.removeTextureFromCache(txt.baseTexture.imageUrl);
            //txt.destroy();
        }
        public static disposeText(txt:PIXI.Text)
        {
            txt.destroy(true);
        }
    }
}