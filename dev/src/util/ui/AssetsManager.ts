///<reference path="../../../lib/PIXI.d.ts" />
///<reference path="../signal/SignalManager.ts" />
///<reference path="../signal/CoreSignals.ts" />

module util.ui {

    import CoreSignals = util.signal.CoreSignals;
    import SignalManager = util.signal.SignalManager;

    export class AssetsManager {

        private static _assetsLoader:PIXI.AssetLoader;

        public static loadAssets(aSpriteSheets:Array<any>)
        {
            var assetsToLoad:Array<string> = new Array<string>();
            for (var i = 0, len = aSpriteSheets.length; i < len; i++)
            {
                assetsToLoad.push("./assets/spritesheets/" + aSpriteSheets[i].id);
            }

            assetsToLoad.push("./assets/images/bg0.jpg");
            assetsToLoad.push("./assets/images/bg1.jpg");
            assetsToLoad.push("./assets/images/bg2.jpg");
            assetsToLoad.push("./assets/images/bg3.jpg");
            assetsToLoad.push("./assets/images/intro_bg.jpg");
            assetsToLoad.push("./assets/images/end_bg_loose.jpg");
            assetsToLoad.push("./assets/images/end_bg.jpg");

            this._assetsLoader = new PIXI.AssetLoader(assetsToLoad, false);
            this._assetsLoader.addEventListener("onComplete", AssetsManager.onAssetsLoaded);
            this._assetsLoader.addEventListener("onProgress", AssetsManager.onAssetsProgress);

            this._assetsLoader.load();
        }

        public static onAssetsProgress = (event:any)=>
        {
            var p:number = 1 - (event.content.content.loadCount / event.content.content.assetURLs.length);
            SignalManager.invokeSignal(CoreSignals.ASSETS_PROGRESS, p);
        }

        public static onAssetsLoaded =()=>
        {
            SignalManager.invokeSignal(CoreSignals.ASSETS_LOADED);
        }
    }
}
