module util {

    export class Clock {
        public static SIMPLE_CLICK : number = 100;

        private static _start : number;

        public static init():void{
            this._start = Date.now();
        }
        public static getTimer():number{
            return (Date.now() - Clock._start);
        }
    }
}