module util.signal {

    export class SignalCallback {

        public cb:Function;
        public priority:number;

        constructor(fCB:Function, iPriority:number) {
            this.cb = fCB;
            this.priority = iPriority;
        }
    }
}