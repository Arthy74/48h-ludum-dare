///<reference path="ASignal.ts" />

module util.signal {

    // Class
    export class SignalManager  {
        private static _signals : Array<ASignal>;
        private static _verbose : boolean = false;

        /**
         *
         */
        public static init() : void
        {
            if ( SignalManager._signals == null )
                SignalManager._signals = new Array<ASignal>();
        }

        public static setVerbose( b : boolean ) : void
        {
            SignalManager._verbose = b;
            for ( var z in this._signals )
            {
                SignalManager._signals[ z ].verbose = b;
            }
        }

        public static getVerbose( ) : boolean
        {
            return this._verbose;
        }

        /**
         * clear all Signals references
         */
        public static clearAll() : void
        {
            for ( var z in SignalManager._signals )
            {
                SignalManager._signals[ z ].dispose();
                SignalManager._signals[ z ] = null;
                delete SignalManager._signals[ z ];
            }
        }

        public static existCallback(signalID : string, cb : Function) : boolean
        {
            if ( !SignalManager._signals[ signalID ] )
                return false;
            return SignalManager._signals[ signalID ].exists(cb);
        }

        public static addCallbackSignal(signalID : string, cb : Function, priority : number = 0) : void
        {
            if ( !SignalManager._signals[ signalID ] )
            {
                // create a new Signal if it does not already exists
                SignalManager._signals[ signalID ] = new ASignal(signalID, SignalManager._verbose);
            }

            SignalManager._signals[ signalID ].addCallback(cb, priority);
        }

        public static removeCallbackSignal(signalID : string, cb : Function) : void
        {
            //log.Logger.log("test signal id ", SignalManager._signals[ signalID ])
            if ( SignalManager._signals[ signalID ] == null ) return;

            SignalManager._signals[ signalID ].removeCallback(cb);

            if ( SignalManager._signals[ signalID ].callbacks.length == 0 )
            {
                // remove the Signal from dictionary if no more callback is associated to it
                delete SignalManager._signals[ signalID ];
            }
        }

        public static invokeSignal(signalID : string, ... args) : void
        {
            if ( !SignalManager._signals[ signalID ] )
                return;

            if (SignalManager._verbose) log.Logger.log(" -- invoking Signal '" + signalID + "'callbacks	" + this._signals[ signalID ].callbacks.length);

            SignalManager._signals[ signalID ].invokeCallbacks(args);
        }

        /**
         * List all current Signals
         */
        public static listSignal() : void
        {
            log.Logger.log("---------- list Signals (START) --------");

            for ( var z in SignalManager._signals )
            {
                log.Logger.log("	" + SignalManager._signals[ z ].toString());
            }
            log.Logger.log("---------- list Signals (END) --------");
        }
    }
}
