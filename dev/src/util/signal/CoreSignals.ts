module util.signal {
    export class CoreSignals {

        public static ON_ENTERFRAME:string = "on_enterframe";

        public static CONFIG_LOADED:string = "config loaded";

        public static ASSETS_PROGRESS:string = "assets progress";
        public static ASSETS_LOADED:string = "assets loaded";

        public static SHOW_REQUEST_SHIELD:string = "show request shield";
        public static HIDE_REQUEST_SHIELD:string = "hide request shield";
    }
}

