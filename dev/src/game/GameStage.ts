///<reference path="../../lib/PIXI.d.ts" />
///<reference path="../../lib/tween.js.d.ts" />
///<reference path="../util/Log.ts" />
///<reference path="../util/signal/SignalManager.ts" />
///<reference path="../util/signal/CoreSignals.ts" />
///<reference path="../game/signal/Signals.ts" />
///<reference path="./AScreen.ts" />
///<reference path="../game/screen/MainScreen.ts" />
///<reference path="../game/screen/LoadingScreen.ts" />
///<reference path="../game/screen/InGameScreen.ts" />
///<reference path="../game/screen/EndGameScreen.ts" />
///<reference path="../game/model/Config.ts" />

module game {

    import SignalManager = util.signal.SignalManager;
    import Signals = game.signal.Signals;
    import Logger = log.Logger;
    import CoreSignals = util.signal.CoreSignals;
    import ScreenEnum = game.model.enums.ScreenEnum;
    import LoadingScreen = game.screen.LoadingScreen;
    import MainScreen = game.screen.MainScreen;
    import InGameScreen = game.screen.InGameScreen;
    import EndGameScreen = game.screen.EndGameScreen;
    import Config = game.model.Config;

    export class GameStage {

        public static currentScreen:AScreen;
        public static renderer: PIXI.PixiRenderer;

        public static width: number;
        public static height: number;
        public static ratio: number;

        private static _stage: PIXI.Stage;

        public static create(displaymode:number, scale:boolean=false, isMobile:boolean=false) {
            if (GameStage.renderer) return this;

            Config.IS_MOBILE = isMobile;

            GameStage.adjustRatio();

            var rendererOptions: Object = {
                antialiasing:false,
                transparent:false,
                resolution:1
            };
            // create a renderer passing in the options
            Logger.log("init pixi mode " + displaymode);

            switch(displaymode){
                case 0:
                    GameStage.renderer = PIXI.autoDetectRenderer(GameStage.width, GameStage.height, rendererOptions);
                    break;
                case 1:
                    GameStage.renderer = new PIXI.CanvasRenderer(GameStage.width, GameStage.height, rendererOptions);
                    break;
                case 2:
                    GameStage.renderer = new PIXI.WebGLRenderer(GameStage.width, GameStage.height, rendererOptions);
                    break;
            }

            document.getElementById('game-content').appendChild(GameStage.renderer.view);

            GameStage._stage = new PIXI.Stage(0xFFFFFF);
            console.log("stage created");

            if (scale) {
                GameStage._rescale();
                window.addEventListener('resize', GameStage._rescale, false);
            }

            requestAnimFrame(GameStage.loop);
            return this;
        }

        private static adjustRatio() {
            GameStage.ratio = Math.min(window.innerWidth/Config.GAME_WIDTH, window.innerHeight/Config.GAME_HEIGHT);

            GameStage.width = Math.round(Config.GAME_WIDTH * GameStage.ratio);
            GameStage.height = Math.round(Config.GAME_HEIGHT * GameStage.ratio);
        }

        private static _rescale=()=> {
            GameStage.adjustRatio();

            GameStage.renderer.resize(GameStage.width, GameStage.height);

            if (GameStage.currentScreen != undefined) {
                GameStage.currentScreen.rescale(GameStage.ratio);
            }
        }

        private static loop() {
            requestAnimFrame(GameStage.loop);

            //stats.begin();
            TWEEN.update();

            if (!GameStage.currentScreen || GameStage.currentScreen.isPaused())
            {
                console.log("no render");
                return;
            }

            SignalManager.invokeSignal(CoreSignals.ON_ENTERFRAME);
            GameStage.renderer.render(GameStage._stage);
        }

        public static goToScreen(id: string): boolean {

            if (GameStage.currentScreen) {
                GameStage._stage.removeChild(GameStage.currentScreen);
                GameStage.currentScreen.onRemoved();
                GameStage.currentScreen = null;
            }

            switch(id)
            {
                case ScreenEnum.LOADING:
                    GameStage.currentScreen = new LoadingScreen();
                    break;
                case ScreenEnum.MAIN:
                    GameStage.currentScreen = new MainScreen();
                    break;
                case ScreenEnum.INGAME:
                    GameStage.currentScreen = new InGameScreen();
                    break;
                case ScreenEnum.END_GAME:
                    GameStage.currentScreen = new EndGameScreen();
                    break;
            }

            if (GameStage.currentScreen != null) {
                GameStage._stage.addChild(GameStage.currentScreen);
                GameStage.currentScreen.onAdded();
            }
            GameStage._rescale();

            return true;
        }
    }
}

