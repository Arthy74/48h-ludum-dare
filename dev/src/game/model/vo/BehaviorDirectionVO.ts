module game.model.vo {
    export class BehaviorDirectionVO {
        public direction:number;
        public stepDuration:number;

        constructor() {
        }

        public randomize(firstValue:boolean)
        {
            this.direction = Math.round(Math.random()*3);

            if (firstValue)
            {
                this.stepDuration = Math.round(2 + Math.random() * 5);
            }
            else
            {
                this.stepDuration = Math.round(32 + Math.random() * 64);
            }
        }
    }
}