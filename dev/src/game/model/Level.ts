///<reference path="../../../lib/PIXI.d.ts" />

module game.model {
    export class Level {

        public rect:PIXI.Rectangle;
        public exit:PIXI.Point;

        public center:PIXI.Point;

        constructor(xx:number, yy:number, ww:number, hh:number) {
            this.setDimension(xx, yy, ww, hh);
        }

        public setDimension(xx:number, yy:number, ww:number, hh:number)
        {
            this.rect = new PIXI.Rectangle(xx, yy, ww, hh);
            this.center = new PIXI.Point(Math.round(xx + ww/2), Math.round(yy + hh/2));
            this.generate();
        }

        public generate() {

            // place exit
            var margin:number = this.rect.width*0.1;
            var xx:number;
            var yy:number;

            if (Math.random()>0.5) {
                // horizontal bound
                xx = this.rect.x + margin + Math.random() * (this.rect.width - 2 * margin);
                if (Math.random()>0.5) yy = this.rect.y + margin;
                else yy = this.rect.y + this.rect.height - margin;
            }
            else
            {
                // vertical bound
                yy = this.rect.y + margin + Math.random()*(this.rect.height - 2*margin);
                if (Math.random()>0.5) xx = this.rect.x + margin;
                else xx = this.rect.x + this.rect.width - margin;
            }

            this.exit = new PIXI.Point(xx, yy);
        }
    }
}