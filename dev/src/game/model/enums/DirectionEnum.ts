module game.model.enums {
    export class DirectionEnum {
        public static DOWN:number = 0;
        public static LEFT:number = 1;
        public static RIGHT:number = 2;
        public static UP:number = 3;
    }
}