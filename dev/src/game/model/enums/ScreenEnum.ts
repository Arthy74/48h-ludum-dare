module game.model.enums {
    export class ScreenEnum {
        public static LOADING:string = "loading";
        public static MAIN:string = "main";
        public static INGAME:string = "ingame";
        public static END_GAME:string = "end_game";
    }
}