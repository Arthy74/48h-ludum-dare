///<reference path="../../../../lib/PIXI.d.ts" />

///<reference path="../../../util/signal/SignalManager.ts"/>
///<reference path="../../signal/Signals.ts"/>
///<reference path="../../../util/ui/CSS.ts"/>
///<reference path="../../../util/ui/CommonButton.ts"/>

module game.screen.ingame {

    import CSS = util.ui.CSS;
    import CommonButton = util.ui.CommonButton;

    export class CharacterChoiceButton extends CommonButton {

        public skinID:number;

        constructor(charSkinID:number) {
            this.skinID = charSkinID;

            var imgIndex:number = (charSkinID * 3) + 1;
            var textureName:string = "char-" + (imgIndex <10 ? "0" + String(imgIndex) : String(imgIndex)) + ".png";
            super(textureName, textureName);

            this.scale = new PIXI.Point(1.5, 1.5);
        }
    }
}
