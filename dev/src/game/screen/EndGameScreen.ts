///<reference path="../../../lib/PIXI.d.ts" />
///<reference path="../../util/Log.ts" />
///<reference path="../AScreen.ts" />
///<reference path="../model/Config.ts"/>
///<reference path="../../util/ui/CSS.ts"/>
///<reference path="../../util/ui/CommonButton.ts"/>
///<reference path="../../util/signal/SignalManager.ts"/>
///<reference path="../signal/Signals.ts"/>
///<reference path="../GameStage.ts"/>
///<reference path="../model/enums/ScreenEnum.ts"/>
///<reference path="../model/vo/GameVO.ts"/>
///<reference path="./common/Credits.ts"/>

module game.screen {
    // Class
    import Logger = log.Logger;
    import CSS = util.ui.CSS;
    import Config = game.model.Config;
    import CommonButton = util.ui.CommonButton;
    import SignalManager = util.signal.SignalManager;
    import Signals = game.signal.Signals;
    import ScreenEnum = game.model.enums.ScreenEnum;
    import GameVO = game.model.vo.GameVO;
    import SoundManager = util.audio.SoundManager;

    export class EndGameScreen extends AScreen {

        private _endBt:CommonButton;
        private _credits:Credits;

        constructor() {
            super();
            this.id = "endgame";
        }

        public onAdded()
        {
            if (Config.CURRENT_GAME == null)
            {
                Config.CURRENT_GAME = new GameVO();
                Config.CURRENT_GAME.playerShouldWin = false;
            }

            var info_txt:string;
            var bg_image:string;
            if (Config.CURRENT_GAME.playerShouldWin) {
                info_txt = Config.DATA.texts.endScreen_win;
                bg_image = "./assets/images/end_bg.jpg";
            }
            else
            {
                info_txt = Config.DATA.texts.endScreen_loose;
                bg_image = "./assets/images/end_bg_loose.jpg";
            }

            var bg:PIXI.Sprite = PIXI.Sprite.fromImage(bg_image);
            this.addChild(bg);

            var font:any = CSS.getFont(CSS.FONT_Pusab, 50, "#cd3937");
            font.wordWrap = true;
            font.wordWrapWidth = 522;

            var title:Title = new Title();
            this.addChild(title);

            var font:any = CSS.getFont(CSS.FONT_Pusab, 30, "#ffffff");
            CSS.addStroke(font, "#272e26", 8);
            font.wordWrap = true;
            font.wordWrapWidth = 522;

            var help:PIXI.Text = new PIXI.Text(info_txt, font);
            help.anchor.x = 0.5;
            help.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 450);
            this.addChild(help);

            this._endBt = new CommonButton("button_out.png", "button_over.png");
            this._endBt.buttonFamily = "end_button";
            this._endBt.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 700);
            this._endBt.setLabel("NEXT");
            this.addChild(this._endBt);

            this._credits = new Credits();
            this.addChild(this._credits);

            if (Config.CURRENT_GAME.playerShouldWin) {
                Config.USER.winCoins(5);
                this._credits.update(true);
            }

            Logger.log("onAdded endgame");
            SignalManager.addCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);

            SoundManager.playGameBG("intro_loop");
            super.onAdded();
        }

        public onRemoved()
        {
            SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);

            this._endBt.dispose();
            this._endBt = null;

            super.onRemoved();
        }

        private onButtonClick=(buttonRef)=>
        {
            Logger.log("onButtonClick endgame");
            if (buttonRef.buttonFamily == "end_button") {
                SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
                GameStage.goToScreen(ScreenEnum.MAIN);
            }
        }
    }
}
