///<reference path="../../../../lib/PIXI.d.ts" />
///<reference path="../../model/Config.ts" />

module game.screen {

    import Config = game.model.Config;
    export class Title extends PIXI.Sprite {

        constructor() {
            super(PIXI.Texture.fromFrame("title_top.png"));

            this.anchor = new PIXI.Point(0.5, 0.5);
            this.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 80);
        }
    }
}