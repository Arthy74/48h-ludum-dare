///<reference path="../../../../lib/PIXI.d.ts" />

///<reference path="../../model/Config.ts" />
///<reference path="../../../util/ui/CSS.ts"/>

module game.screen {

    import Config = game.model.Config;
    import CSS = util.ui.CSS;

    export class Credits extends PIXI.DisplayObjectContainer {

        private _creditsTF : PIXI.Text;

        constructor() {
            super();

            var coin:PIXI.Sprite = PIXI.Sprite.fromFrame("coin.png");
            coin.anchor = new PIXI.Point(0.5, 0.5);
            this.addChild(coin);

            var font:any = CSS.getFont(CSS.FONT_Pusab, 50, "#ffffff");
            CSS.addStroke(font, "#f99803", 4);

            this._creditsTF = new PIXI.Text(String(Config.USER.getCredits()), font);
            this._creditsTF.anchor.x = 0;
            this._creditsTF.anchor.y = 0.5;
            this._creditsTF.position = new PIXI.Point(50, 0);
            this.addChild(this._creditsTF);

            this.position = new PIXI.Point(50, Config.GAME_HEIGHT - 50);
        }

        public update(animated:boolean = false)
        {
            this._creditsTF.setText(String(Config.USER.getCredits()));
        }
    }
}