/// <reference path="../lib/PIXI.d.ts" />
/// <reference path="../lib/tween.js.d.ts" />
/// <reference path="../lib/howler.d.ts" />
declare module log {
    class Logger {
        private static start;
        private static logLayer;
        private static logText;
        private static logBG;
        private static logHistory;
        private static logHistoryMax;
        static verbose: boolean;
        static init(verbose: boolean): void;
        static log(...args: any[]): void;
        static addOnscreenLogger(container: PIXI.DisplayObjectContainer): void;
        static onChangeScale(): void;
        private static redraw(xx, yy, ww, hh);
    }
}
declare module util {
    class Clock {
        static SIMPLE_CLICK: number;
        private static _start;
        static init(): void;
        static getTimer(): number;
    }
}
declare module util.signal {
    class SignalCallback {
        public cb: Function;
        public priority: number;
        constructor(fCB: Function, iPriority: number);
    }
}
declare module util.signal {
    class ASignal {
        public id: string;
        public callbacks: SignalCallback[];
        public verbose: boolean;
        private _idx;
        constructor(id_message: string, verbose?: boolean);
        public dispose(): void;
        public addCallback(cb: Function, priority?: number): void;
        private sortCallBack(a, b);
        public removeCallback(cb: Function): void;
        public removeALLCallbacks(): void;
        public exists(cb: Function): boolean;
        public invokeCallbacks(args: SignalCallback[]): void;
        public toString(): String;
        private getItemIndex(ref);
    }
}
declare module util.signal {
    class SignalManager {
        private static _signals;
        private static _verbose;
        static init(): void;
        static setVerbose(b: boolean): void;
        static getVerbose(): boolean;
        static clearAll(): void;
        static existCallback(signalID: string, cb: Function): boolean;
        static addCallbackSignal(signalID: string, cb: Function, priority?: number): void;
        static removeCallbackSignal(signalID: string, cb: Function): void;
        static invokeSignal(signalID: string, ...args: any[]): void;
        static listSignal(): void;
    }
}
declare module util.signal {
    class CoreSignals {
        static ON_ENTERFRAME: string;
        static CONFIG_LOADED: string;
        static ASSETS_PROGRESS: string;
        static ASSETS_LOADED: string;
        static SHOW_REQUEST_SHIELD: string;
        static HIDE_REQUEST_SHIELD: string;
    }
}
declare module game.signal {
    class Signals {
        static DEFAULT_BUTTON_CLICK: string;
        static REACHED_EXIT: string;
    }
}
declare module util {
    class DisposeUtil {
        static dispose(container: PIXI.DisplayObjectContainer): void;
        static destroyTexture(txt: PIXI.Texture): void;
        static disposeText(txt: PIXI.Text): void;
    }
}
declare module game {
    class AScreen extends PIXI.Stage {
        static COUNT: number;
        public id: string;
        public internalID: number;
        public paused: boolean;
        public built: boolean;
        constructor();
        public rescale(): void;
        public onRemoved(): void;
        public onAdded(): void;
        public pause(): void;
        public resume(): void;
        public isPaused(): boolean;
    }
}
declare module util.ui {
    class CSS {
        static FONT_CandelaBold: string;
        static FONT_Pusab: string;
        private static styles;
        static getFont(id: string, size: number, color: string, align?: string): Object;
        static addDropShadow(obj: any, color: string, angle?: number, distance?: number): void;
        static addStroke(obj: any, color: string, thickness: number): void;
        static alignInRect(txt: PIXI.Text, rect: PIXI.Rectangle, alignX: string, alignY: string): void;
    }
}
declare module util.ui {
    class CommonButton extends PIXI.DisplayObjectContainer {
        public buttonValue: string;
        public buttonFamily: String;
        public buttonIndex: number;
        public _selectedSprite: PIXI.Sprite;
        public textureOn: PIXI.Texture;
        public textureOff: PIXI.Texture;
        public tween: TWEEN.Tween;
        public clickData: PIXI.InteractionData;
        public selectedReplaceOver: boolean;
        private _over;
        private _offExtra;
        private _selected;
        private _base;
        private _clickSignal;
        private _wasDown;
        private _active;
        private _label;
        constructor(baseTxtName: string, overTxtName: string, active?: boolean);
        public disposeLabel(): void;
        public setLabel(value: string): void;
        public dispose(): void;
        public setBaseTexture(txt: PIXI.Texture): void;
        public setOverTexture(txt: PIXI.Texture): void;
        public offsetOver(xx: number, yy: number): void;
        public setOffTexture(offTxt: PIXI.Texture): void;
        public offsetOffTexture(xx: number, yy: number): void;
        public setSignal(id: string): void;
        public setActive(b: boolean, alterTextures?: boolean): void;
        private updateButtonState(b, alterTextures?);
        public select(b: boolean): void;
        private updateSelectedState();
        private onMouseOver(data);
        private onMouseOut(data);
        private onMouseDown(data);
        public onMouseUp: (data: any) => void;
    }
}
declare module game.model.enums {
    class ScreenEnum {
        static LOADING: string;
        static MAIN: string;
        static INGAME: string;
        static END_GAME: string;
    }
}
declare module util.io {
    class IORequest {
        public silenceMode: boolean;
        private _request;
        private _success;
        private _fail;
        private _method;
        private _url;
        private _data;
        constructor(url: string, method?: string, data?: Object);
        public sendRequest(cbSuccess: Function, cbfail?: Function): void;
        public onGetAnswer: (evt: Event) => void;
        public onError: (evt: Event) => void;
    }
}
declare module game.model.vo {
    class GameVO {
        public selectedSkinID: number;
        public predictedWinnerID: number;
        public playerShouldWin: boolean;
        constructor();
    }
}
declare module game.model {
    class Config {
        static DATA: any;
        static IS_MOBILE: boolean;
        static GAME_WIDTH: number;
        static GAME_HEIGHT: number;
        static CURRENT_GAME: vo.GameVO;
        static init(): void;
        static onGetConfigSuccess: (json: any) => void;
        static onGetConfigFail: (data: string) => void;
    }
}
declare module game.screen {
    class Title extends PIXI.Sprite {
        constructor();
    }
}
declare module game.screen {
    class MainScreen extends AScreen {
        private _startBt;
        constructor();
        public onRemoved(): void;
        public onAdded(): void;
        private onButtonClick;
        public rescale(): void;
    }
}
declare module util.audio {
    class ASoundEngine {
        public audioPath: String;
        public init(audioJson: Object, isMobile: boolean): void;
        public playSound(id: string): void;
        public muteSounds(b: boolean): void;
    }
}
declare module util.audio.howler {
    class HowlerSoundVO {
        public id: string;
        public url: string;
        public preload: boolean;
        public autoplay: boolean;
        public volume: number;
        public loop: boolean;
        public formats: string[];
        constructor(data: any, formats: string[]);
    }
}
declare module util.audio.howler {
    class HowlerSound {
        public data: HowlerSoundVO;
        private _soundInstance;
        private _engineRef;
        private _aSrc;
        private _soundBuilt;
        private _isMuted;
        constructor(engine: HowlerEngine, data: HowlerSoundVO);
        private buildSound();
        private onSoundComplete;
        public play(): void;
        public onSoundLoaded: () => void;
        public mute(b: boolean): void;
        private updateMuted();
    }
}
declare module util.audio.howler {
    class HowlerEngine extends ASoundEngine {
        private _loadingCount;
        private _HowlerSounds;
        private _formats;
        public init(audioJson: any, isMobile: boolean): void;
        public muteSounds(b: boolean): void;
        public onSoundPreloaded(): void;
        public playSound(id: string): void;
    }
}
declare module util.audio.signal {
    class SoundSignals {
        static SOUND_MUTED: string;
        static SOUNDS_READY: string;
    }
}
declare module util.audio {
    class SoundManager {
        private static _ready;
        private static _playingBG;
        private static _engine;
        static init(audioJson: Object, isMobile: boolean): void;
        static onEngineReady(): void;
        static onMuteSound: (b: boolean) => void;
        static setMobile(): void;
        static playGameBG(): void;
        static playSound(id: string): void;
    }
}
declare module util.ui {
    class AssetsManager {
        private static _assetsLoader;
        static loadAssets(aSpriteSheets: any[]): void;
        static onAssetsProgress: (event: any) => void;
        static onAssetsLoaded: () => void;
    }
}
declare module game.screen {
    class LoadingScreen extends AScreen {
        private _progress;
        constructor();
        public onAdded(): void;
        public onRemoved(): void;
        private onConfigLoaded;
        public onAssetsProgress: (percent: number) => void;
        public onAssetsLoaded: () => void;
        private onSoundsReady;
        public rescale(): void;
    }
}
declare module game.model {
    class Level {
        public rect: PIXI.Rectangle;
        public exit: PIXI.Point;
        public center: PIXI.Point;
        constructor(xx: number, yy: number, ww: number, hh: number);
        public setDimension(xx: number, yy: number, ww: number, hh: number): void;
        public generate(): void;
    }
}
declare module game.model.vo {
    class BehaviorDirectionVO {
        public direction: number;
        public stepDuration: number;
        constructor();
        public randomize(): void;
    }
}
declare module game.model {
    class CharacterBehavior {
        private _aDirection;
        private _simulationMode;
        private _playBack;
        private _playBackCount;
        constructor();
        public dispose(): void;
        public simulationSteps(): number;
        public gameSteps(): number;
        public startSimulation(): void;
        public startGame(): void;
        public getNextDirection(): vo.BehaviorDirectionVO;
    }
}
declare module game.model.enums {
    class DirectionEnum {
        static DOWN: number;
        static LEFT: number;
        static RIGHT: number;
        static UP: number;
    }
}
declare module game.screen.ingame {
    class Treasure extends PIXI.DisplayObjectContainer {
        constructor();
    }
}
declare module game.screen.ingame {
    class Character extends PIXI.DisplayObjectContainer {
        static CHARACTER_FRAME: PIXI.Rectangle;
        static CHARACTERS_TEXTURE: PIXI.Texture;
        public charID: number;
        public charSkinID: number;
        public behavior: model.CharacterBehavior;
        private _animation;
        private _walkLeft;
        private _walkRight;
        private _walkUp;
        private _walkDown;
        private _direction;
        private _speed;
        private _refLevel;
        private _nextChangeDirection;
        private _exitInSight;
        private _distanceToExit;
        private _foundExit;
        private _treasureIcon;
        constructor(num: number);
        public dispose(): void;
        public setLevel(refLevel: model.Level): void;
        public init(): void;
        public setCharacter(num: number): void;
        public changeDirection(direction: number): void;
        public onStep: () => void;
        private aimExit();
        private checkBounds();
        private checkPosition();
        private distanceToExit();
        private distanceToExitX();
        private distanceToExitY();
        public changeRandom(): void;
        private getAnimation(direction);
    }
}
declare module game.screen.ingame {
    class CharacterChoiceButton extends util.ui.CommonButton {
        public skinID: number;
        constructor(charSkinID: number);
    }
}
declare module game.screen.ingame {
    class CharacterChoice extends PIXI.DisplayObjectContainer {
        public tween: TWEEN.Tween;
        private _yOutside;
        private _coin;
        private _aButton;
        constructor();
        public build(): void;
        public dispose(): void;
        public selectChar(bt: CharacterChoiceButton): void;
        public show(): void;
        public hide(): void;
    }
}
declare module game.screen {
    class InGameScreen extends AScreen {
        private _level;
        private _aChar;
        private _treasure;
        private _gameOver;
        private _simulationMode;
        private _choice;
        private _close;
        constructor();
        public onAdded(): void;
        private reset();
        public onRemoved(): void;
        private simulateGame();
        public onEnterFrameSimulation: () => void;
        private endOfSimulation();
        private startGame();
        private getCharsIndex();
        private gapIndex(value);
        public onButtonClick: (buttonRef: any) => void;
        public onEnterFrame: () => void;
        private endOfGame();
        private updateCharacters();
        public onCharacterWin: (char: ingame.Character) => void;
        public rescale(): void;
    }
}
declare module game.screen {
    class EndGameScreen extends AScreen {
        private _endBt;
        constructor();
        public onAdded(): void;
        public onRemoved(): void;
        private onButtonClick;
        public rescale(): void;
    }
}
declare module game {
    class GameStage {
        static currentScreen: AScreen;
        static renderer: PIXI.PixiRenderer;
        static width: number;
        static height: number;
        static create(width: number, height: number, displaymode: number, scale?: boolean, mobilePortrait?: boolean): typeof GameStage;
        private static _rescale();
        private static loop();
        static goToScreen(id: string): boolean;
    }
}
declare function init(): void;
