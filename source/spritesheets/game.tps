<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.8.0</string>
        <key>fileName</key>
        <string>/Users/arthy/gamesys-ludum-dare/source/spritesheets/game.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename>../../dev/bin/assets/spritesheets/ss_0.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../dev/bin/assets/spritesheets/ss_0.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../images/button_out.png</filename>
            <filename>../images/button_over.png</filename>
            <filename>../images/hat.png</filename>
            <filename>../images/characters/char-02.png</filename>
            <filename>../images/characters/char-03.png</filename>
            <filename>../images/characters/char-04.png</filename>
            <filename>../images/characters/char-05.png</filename>
            <filename>../images/characters/char-06.png</filename>
            <filename>../images/characters/char-07.png</filename>
            <filename>../images/characters/char-08.png</filename>
            <filename>../images/characters/char-09.png</filename>
            <filename>../images/characters/char-10.png</filename>
            <filename>../images/characters/char-11.png</filename>
            <filename>../images/characters/char-12.png</filename>
            <filename>../images/characters/char-13.png</filename>
            <filename>../images/characters/char-14.png</filename>
            <filename>../images/characters/char-15.png</filename>
            <filename>../images/characters/char-16.png</filename>
            <filename>../images/characters/char-17.png</filename>
            <filename>../images/characters/char-18.png</filename>
            <filename>../images/characters/char-19.png</filename>
            <filename>../images/characters/char-20.png</filename>
            <filename>../images/characters/char-21.png</filename>
            <filename>../images/characters/char-22.png</filename>
            <filename>../images/characters/char-23.png</filename>
            <filename>../images/characters/char-24.png</filename>
            <filename>../images/characters/char-25.png</filename>
            <filename>../images/characters/char-26.png</filename>
            <filename>../images/characters/char-27.png</filename>
            <filename>../images/characters/char-28.png</filename>
            <filename>../images/characters/char-29.png</filename>
            <filename>../images/characters/char-30.png</filename>
            <filename>../images/characters/char-31.png</filename>
            <filename>../images/characters/char-32.png</filename>
            <filename>../images/characters/char-33.png</filename>
            <filename>../images/characters/char-34.png</filename>
            <filename>../images/characters/char-35.png</filename>
            <filename>../images/characters/char-36.png</filename>
            <filename>../images/characters/char-37.png</filename>
            <filename>../images/characters/char-38.png</filename>
            <filename>../images/characters/char-39.png</filename>
            <filename>../images/characters/char-40.png</filename>
            <filename>../images/characters/char-41.png</filename>
            <filename>../images/characters/char-42.png</filename>
            <filename>../images/characters/char-43.png</filename>
            <filename>../images/characters/char-44.png</filename>
            <filename>../images/characters/char-45.png</filename>
            <filename>../images/characters/char-46.png</filename>
            <filename>../images/characters/char-47.png</filename>
            <filename>../images/characters/char-48.png</filename>
            <filename>../images/characters/char-49.png</filename>
            <filename>../images/characters/char-50.png</filename>
            <filename>../images/characters/char-51.png</filename>
            <filename>../images/characters/char-52.png</filename>
            <filename>../images/characters/char-53.png</filename>
            <filename>../images/characters/char-54.png</filename>
            <filename>../images/characters/char-55.png</filename>
            <filename>../images/characters/char-56.png</filename>
            <filename>../images/characters/char-57.png</filename>
            <filename>../images/characters/char-58.png</filename>
            <filename>../images/characters/char-59.png</filename>
            <filename>../images/characters/char-60.png</filename>
            <filename>../images/characters/char-61.png</filename>
            <filename>../images/characters/char-62.png</filename>
            <filename>../images/characters/char-63.png</filename>
            <filename>../images/characters/char-64.png</filename>
            <filename>../images/characters/char-65.png</filename>
            <filename>../images/characters/char-66.png</filename>
            <filename>../images/characters/char-67.png</filename>
            <filename>../images/characters/char-68.png</filename>
            <filename>../images/characters/char-69.png</filename>
            <filename>../images/characters/char-70.png</filename>
            <filename>../images/characters/char-71.png</filename>
            <filename>../images/characters/char-72.png</filename>
            <filename>../images/characters/char-73.png</filename>
            <filename>../images/characters/char-74.png</filename>
            <filename>../images/characters/char-75.png</filename>
            <filename>../images/characters/char-76.png</filename>
            <filename>../images/characters/char-77.png</filename>
            <filename>../images/characters/char-78.png</filename>
            <filename>../images/characters/char-79.png</filename>
            <filename>../images/characters/char-80.png</filename>
            <filename>../images/characters/char-81.png</filename>
            <filename>../images/characters/char-82.png</filename>
            <filename>../images/characters/char-83.png</filename>
            <filename>../images/characters/char-84.png</filename>
            <filename>../images/characters/char-85.png</filename>
            <filename>../images/characters/char-86.png</filename>
            <filename>../images/characters/char-87.png</filename>
            <filename>../images/characters/char-88.png</filename>
            <filename>../images/characters/char-89.png</filename>
            <filename>../images/characters/char-90.png</filename>
            <filename>../images/characters/char-91.png</filename>
            <filename>../images/characters/char-92.png</filename>
            <filename>../images/characters/char-93.png</filename>
            <filename>../images/characters/char-94.png</filename>
            <filename>../images/characters/char-95.png</filename>
            <filename>../images/characters/char-96.png</filename>
            <filename>../images/characters/char-01.png</filename>
            <filename>../images/treasure.png</filename>
            <filename>../images/panel_choice.png</filename>
            <filename>../images/coin.png</filename>
            <filename>../images/close.png</filename>
            <filename>../images/title_top.png</filename>
            <filename>../images/Treasure_big.png</filename>
            <filename>../images/exclamation.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>2</uint>
        <key>commonDivisorY</key>
        <uint>2</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
