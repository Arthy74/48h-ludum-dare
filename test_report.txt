Friday 19 - 12pm
Installing dev environment
Extracting libraries from personal project to build up a game template with:
	- signal messaging
	- logging 
	- spritesheets
	- audio
	- screens handler

Problems:
Pixi.d.ts cannot be compiled through Webstom, using a previous version


Saturday
Looking for loops (ingame and intro)
using audacity for converting to ogg

lost lot of time trying to use a spritesheet texture to crop individual sprites
-> turns out that my version of Pixi doesn't support crop parameter
-> used photoshop slice tools to get individual sprite to png and insert them in the main spritesheet

added character animation
added random movement system for characters

successfully implemented a predictive mechanism to know in advance which character will find the treasure first
added player choice panel
set character skin ID depending on user choice and final result

Sunday
Refactored screens UI
improved predictive settings in config
added user and credits system
prepared dev/prod html pages

added sound engine (howler)
added loops and sounds

fixed resize to fit 640x960
added mobile detection

Credits:
background:	https://www.pinterest.com/nganlee/game-background/
sounds: eternal flaskit.com
